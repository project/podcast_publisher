<?php

namespace Drupal\Tests\podcast_publisher\Functional;

/**
 * Tests generated podcast feed.
 *
 * @group podcast_publisher
 */
class PodcastPublisherFeedTest extends PodcastPublisherFunctionalTestBase {

  use PodcastPublisherTestContentGenerationTrait;

  /**
   * The feed object.
   *
   * @var \SimpleXMLElement
   */
  protected $feed;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $page = $this->getSession()->getPage();

    // Prepare feed.
    $podcast = $this->generatePodcast();
    $this->generatePodcastEpisodes($podcast);
    $this->drupalGet('/podcast/' . $podcast->id() . '/feed.xml');
    $this->feed = simplexml_load_string($page->getContent());
  }

  /**
   * Test the channel fields.
   */
  public function testChannelFields() {
    // Test podcast fields.
    /** @var \SimpleXMLElement[] $channel */
    $channel = $this->feed->xpath('/rss/channel');
    $channel = reset($channel);
    $this->assertEquals('TestPodcast', $channel->title);
    $this->assertEquals('This is a summary.', $channel->description);
    $this->assertEquals('en', $channel->language);
    $this->assertEquals('Podcast', $channel->category);

    // Test itunes fields.
    $itunes_elements = $channel->children('itunes', TRUE);
    $this->assertEquals(\Drupal::currentUser()->getAccountName(), $itunes_elements->author);
    $this->assertEquals('episodical', $itunes_elements->type);
    $this->assertEquals('Überpodcast', $itunes_elements->subtitle);
    $this->assertEquals('No', $itunes_elements->complete);
    $this->assertEquals('False', $itunes_elements->explicit);
    $this->assertStringContainsString('/files/styles/podcast_3000x3000/public/', $itunes_elements->image->attributes()->href);

    $expected_categories = [
      'Arts' => 'Performing Arts',
      'Health & Fitness' => 'Fitness',
    ];
    $feed_categories = $itunes_elements->category;
    /** @var \SimpleXMLElement $result */
    foreach ($feed_categories as $category) {
      $parent_category = (string) $category->attributes()->text;
      $this->assertArrayHasKey($parent_category, $expected_categories);
      $feed_category = (string) $category->children('itunes', TRUE)[0]->attributes()->text;
      $this->assertStringContainsString($expected_categories[$parent_category], $feed_category);
    }

    $owner = $itunes_elements->owner->children('itunes', TRUE);
    $this->assertEquals(\Drupal::currentUser()->getAccountName(), $owner->name);
    $this->assertEquals('popu@example.com', $owner->email);
  }

  /**
   * Test the item fields.
   */
  public function testItemFields() {
    /** @var \SimpleXMLElement[] $channel */
    $channel = $this->feed->xpath('/rss/channel');
    $channel = reset($channel);

    // Episodes should be ordered by date - most recent first.
    [$episode_three, $episode_two, $episode_one] = $channel->item;

    // Start with the first episode / second item.
    $file_uri_regex = '/^https?\:\/\/.*files\/sample-3s-1\.mp3$/';

    // Test enclosure.
    $enclosure_attrs = $episode_one->enclosure->attributes();
    $this->assertMatchesRegularExpression($file_uri_regex, $enclosure_attrs->url);
    $this->assertEquals('audio/mpeg', $enclosure_attrs->type);
    $this->assertEquals('52079', $enclosure_attrs->length);

    $this->assertMatchesRegularExpression($file_uri_regex, $episode_one->guid);

    // Plain text formatter surrounds text with p tag and adds a new line.
    $this->assertEquals("<p>This is the first episode.</p>\n", $episode_one->description);

    // Test itunes fields.
    $itunes_elements = $episode_one->children('itunes', TRUE);
    $this->assertEquals('True', $itunes_elements->explicit);
    $this->assertEquals('0:03', $itunes_elements->duration);
    $this->assertEquals('1', $itunes_elements->episode);
    $this->assertEquals('Episode 1 - The explicit one', $itunes_elements->title);

    // Test second episode.
    $file_uri_regex = '/^https?\:\/\/.*files\/sample-3s-2\.mp3$/';

    // Test enclosure.
    $enclosure_attrs = $episode_two->enclosure->attributes();
    $this->assertMatchesRegularExpression($file_uri_regex, $enclosure_attrs->url);
    $this->assertEquals('audio/mpeg', $enclosure_attrs->type);
    $this->assertEquals('52079', $enclosure_attrs->length);

    $this->assertMatchesRegularExpression($file_uri_regex, $episode_two->guid);

    // Plain text formatter surrounds text with p tag and adds a new line.
    $this->assertEquals("<p>This is the second episode.</p>\n", $episode_two->description);

    // Test itunes fields.
    $itunes_elements = $episode_two->children('itunes', TRUE);
    $this->assertEquals('False', $itunes_elements->explicit);
    $this->assertEquals('0:03', $itunes_elements->duration);
    $this->assertEquals('2', $itunes_elements->episode);
    $this->assertEquals('Episode 2 - The safe one', $itunes_elements->title);
  }

}
