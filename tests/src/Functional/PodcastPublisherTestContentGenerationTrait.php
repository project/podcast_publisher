<?php

namespace Drupal\Tests\podcast_publisher\Functional;

use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\podcast_publisher\PodcastInterface;

/**
 * Provides methods to create test content.
 *
 * This trait is meant to be used only by test classes.
 */
trait PodcastPublisherTestContentGenerationTrait {

  use TestFileCreationTrait;

  /**
   * Generates a podcast entity.
   *
   * @return \Drupal\podcast_publisher\PodcastInterface
   *   The entity.
   */
  protected function generatePodcast(): PodcastInterface {
    $user = $this->container->get('current_user');
    $entity_type_manager = $this->container->get('entity_type.manager');
    // Create and upload a file to the media.
    $file = $entity_type_manager->getStorage('file')->create([
      'uri' => current($this->getTestFiles('image'))->uri,
    ]);
    $file->save();
    $itunes_categories = $entity_type_manager->getStorage('taxonomy_term')
      ->loadByProperties([
        'name' => ['Performing Arts', 'Fitness'],
      ]);
    $podcast = $entity_type_manager->getStorage('podcast')
      ->create([
        'title' => 'TestPodcast',
        'authors' => [$user->id()],
        'category' => 'Podcast',
        'complete' => FALSE,
        'copyright' => 'PodcastPublisher Ltd.',
        'email' => 'popu@example.com',
        'explicit' => FALSE,
        'funding' => 'https://paypal.com',
        'image' => $file,
        'itunes_category' => $itunes_categories,
        'mnemonic' => 'TP',
        'subtitle' => 'Überpodcast',
        'summary' => 'This is a summary.',
        'type' => 'episodical',
      ]);
    // Podcast need to be saved in order to create route to feed view.
    $podcast->save();
    return $podcast;
  }

  /**
   * Generates 2 podcast episodes connected to given podcast.
   *
   * @param \Drupal\podcast_publisher\PodcastInterface $podcast
   *   The podcast.
   *
   * @return \Drupal\media\MediaInterface[]
   *   The two generated podcast episodes.
   */
  protected function generatePodcastEpisodes(PodcastInterface $podcast): array {
    $podcast_episodes_data = [];
    $episodes = [];

    $files = $this->getTestAudioFiles();

    // Sort files to make sure episode one uses sample-3s.mp3 and
    // episode 2 uses sample-3s-2.mp3.
    sort($files, SORT_ASC);

    $current_time = time();

    $podcast_episodes_data[] = [
      'created' => $current_time - 1,
      'label' => 'Episode 1 - The explicit one',
      'bundle' => 'podcast_episode',
      'episode_number' => 1,
      'explicit' => TRUE,
      'podcast' => $podcast,
      'shownotes' => [
        'value' => 'This is the first episode.',
        'format' => 'plain_text',
      ],
    ];

    $podcast_episodes_data[] = [
      'created' => $current_time,
      'label' => 'Episode 2 - The safe one',
      'bundle' => 'podcast_episode',
      'episode_number' => 2,
      'explicit' => FALSE,
      'podcast' => $podcast,
      'shownotes' => [
        'value' => 'This is the second episode.',
        'format' => 'plain_text',
      ],
    ];

    $podcast_episodes_data[] = [
      'created' => $current_time + 1,
      'label' => 'Episode 3 - The long one',
      'bundle' => 'podcast_episode',
      'episode_number' => 3,
      'explicit' => FALSE,
      'podcast' => $podcast,
      'shownotes' => [
        'value' => 'This is the third episode.',
        'format' => 'plain_text',
      ],
    ];

    $entity_type_manager = $this->container->get('entity_type.manager');
    $episode_storage = $entity_type_manager->getStorage('podcast_episode');

    foreach ($podcast_episodes_data as $podcast_episode_data) {
      $file = $entity_type_manager->getStorage('file')->create([
        'uri' => array_shift($files)->uri,
      ]);
      $file->save();
      $podcast_episode_data['audio_file'] = $file;
      $episode = $episode_storage->create($podcast_episode_data);
      $episode->save();
      $episodes[] = $episode;
    }

    return $episodes;
  }

  /**
   * Gets test audio files.
   *
   * @return array
   *   The files.
   */
  protected function getTestAudioFiles() {
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    // Copy other test files from fixtures.
    $original = \Drupal::service('module_handler')
      ->getModule('podcast_publisher')
      ->getPath() . '/tests/fixtures/files';
    $files = $file_system->scanDirectory($original, '/^sample.*\.mp3$/');
    foreach ($files as $file) {
      $file_system->copy($file->uri, PublicStream::basePath());
    }

    return $file_system->scanDirectory('public://', '/^sample.*\.mp3$/');
  }

}
