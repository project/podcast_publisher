<?php

namespace Drupal\Tests\podcast_publisher\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * Base class for Podcast functional tests.
 */
abstract class PodcastPublisherFunctionalTestBase extends BrowserTestBase {

  use SchemaCheckTestTrait;
  use RequirementsPageTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'node',
    'field_ui',
    'views_ui',
    'media',
    'media_library',
    'block',
    'text',
    'podcast_publisher',
  ];

  /**
   * Permissions for the admin user that will be logged-in for test.
   *
   * @var array
   */
  protected static $editorUserPermissions = [
    // Podcast publisher module permissions.
    'access podcast overview',
    'administer podcast',
    'view podcast',
    'create podcast',
    'edit podcast',
    'delete podcast',
    // Other permissions.
    'administer views',
    'access content overview',
    'view all revisions',
    'administer content types',
    'administer node fields',
    'administer node form display',
    'administer node display',
    'bypass node access',
  ];

  /**
   * An editor test user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $editorUser;

  /**
   * A non-editor test user account.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $nonAdminUser;

  /**
   * The storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Have two users ready to be used in tests.
    $this->editorUser = $this->drupalCreateUser(static::$editorUserPermissions);
    $this->nonAdminUser = $this->drupalCreateUser([]);
    // Start off logged in as editor.
    $this->drupalLogin($this->editorUser);

    $this->storage = $this->container->get('entity_type.manager')->getStorage('podcast');
  }

}
