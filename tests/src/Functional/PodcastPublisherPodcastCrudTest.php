<?php

namespace Drupal\Tests\podcast_publisher\Functional;

/**
 * Tests podcast publisher CRUD operations.
 *
 * @group podcast_publisher
 */
class PodcastPublisherPodcastCrudTest extends PodcastPublisherFunctionalTestBase {

  /**
   * Test CRUD operations.
   */
  public function testCreatePodcastEntity() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Test creating a podcast entity.
    $this->drupalGet('/admin/content/podcast/add');
    $page->fillField('Title', 'PodcastTest');
    $page->selectFieldOption('Type', 'episodical');
    $page->pressButton('Save');
    $assert_session->pageTextContains('New podcast PodcastTest has been created.');

    // Test editing the podcast entity.
    $page->clickLink('Edit');
    $page->fillField('Title', 'PodcastTest edited');
    $page->pressButton('Save');
    $assert_session->pageTextContains('The podcast PodcastTest edited has been updated.');

    // Test deleting the podcast entity.
    $page->clickLink('Delete');
    $page->pressButton('Delete');
    $assert_session->pageTextContains('The podcast PodcastTest edited has been deleted.');
  }

}
