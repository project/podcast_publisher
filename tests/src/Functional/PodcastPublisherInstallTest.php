<?php

namespace Drupal\Tests\podcast_publisher\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests podcast publisher Install / Uninstall logic.
 *
 * @group podcast_publisher
 */
class PodcastPublisherInstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['podcast_publisher'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser(['administer modules']));
  }

  /**
   * Tests reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Uninstall the media module.
    $this->container->get('module_installer')->uninstall(['podcast_publisher'], FALSE);

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[podcast_publisher][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextContains('Module Podcast Publisher has been installed');
  }

}
