<?php

namespace Drupal\Tests\podcast_publisher\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\views\Views;

/**
 * Base class for Podcast analytics functional tests.
 *
 * @group podcast_publisher
 */
class PodcastPublisherAnalyticsFunctionalTest extends BrowserTestBase {

  use RequirementsPageTrait;
  use PodcastPublisherTestContentGenerationTrait;

  const GOOGLE_CHROME_USER_AGENT_STRING = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36';
  const SPOTIFY_USER_AGENT_STRING = 'Spotify/1.0';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'node',
    'field_ui',
    'views_ui',
    'media',
    'media_library',
    'block',
    'text',
    'podcast_publisher_analytics',
  ];

  /**
   * Permissions for the admin user that will be logged-in for test.
   *
   * @var array
   */
  protected static $editorUserPermissions = [
    // Podcast publisher module permissions.
    'access podcast overview',
    'administer podcast',
    'view podcast',
    'create podcast',
    'edit podcast',
    'delete podcast',
    'administer podcast episode',
    // Other permissions.
    'administer views',
    'access content overview',
    'view all revisions',
    'administer content types',
    'administer node fields',
    'administer node form display',
    'administer node display',
    'bypass node access',
  ];

  /**
   * An editor test user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $editorUser;

  /**
   * A non-editor test user account.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $nonAdminUser;

  /**
   * The storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The podcast's rss feed.
   *
   * @var \1|false|\SimpleXMLElement
   */
  protected $feed;

  /**
   * The queue.
   *
   * @var \Drupal\Core\Queue\QueueFactory|object|null
   */
  protected $queue;

  /**
   * The download intent tracker queue worker.
   *
   * @var \Drupal\Core\Queue\QueueWorkerInterface
   */
  protected $downloadIntentTrackerQueueWorker;

  /**
   * The test podcast entity.
   *
   * @var \Drupal\podcast_publisher\PodcastInterface
   */
  protected $podcast;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Use tracking url in feed.
    $feed_view = Views::getView('podcast_feed');
    $feed_view->setHandlerOption('default', 'field', 'audio_file__target_id', 'type', 'podcast_publisher_download_tracker');
    $feed_view->save();

    // Have two users ready to be used in tests.
    $this->editorUser = $this->drupalCreateUser(static::$editorUserPermissions);
    $this->nonAdminUser = $this->drupalCreateUser([]);
    // Start off logged in as editor.
    $this->drupalLogin($this->editorUser);

    $this->storage = $this->container->get('entity_type.manager')->getStorage('podcast');

    $page = $this->getSession()->getPage();

    // Prepare feed.
    $this->podcast = $this->generatePodcast();
    $this->generatePodcastEpisodes($this->podcast);
    $this->drupalGet('/podcast/' . $this->podcast->id() . '/feed.xml');
    $this->feed = simplexml_load_string($page->getContent());
    $this->queue = $this->container->get('queue');
    $this->downloadIntentTrackerQueueWorker = $this->container->get('plugin.manager.queue_worker')
      ->createInstance('podcast_download_intent_tracker');
  }

  /**
   * Test request count.
   */
  public function testRequestCount() {
    /** @var \SimpleXMLElement[] $channel */
    $channel = $this->feed->xpath('/rss/channel');
    $channel = reset($channel);

    // Episodes should be ordered by date - most recent first.
    [$episode_three, $episode_two, $episode_one] = $channel->item;

    $enclosure_attrs = $episode_one->enclosure->attributes();
    $url = (string) $enclosure_attrs->url;
    $this->assertNotEmpty($url);
    if ($base_url = getenv('SIMPLETEST_BASE_URL')) {
      $url = parse_url($url);
      $url = $base_url . $url['path'];
    }

    // Test a simple GET request.
    $this->getHttpClient()->request('GET', $url);
    $download_intent_count = $this->getDownloadIntentCountFromViewPage(1);
    $this->assertEquals(1, $download_intent_count);

    // HEAD requests should not count.
    $this->getHttpClient()->request('HEAD', $url);
    $download_intent_count = $this->getDownloadIntentCountFromViewPage(1);
    $this->assertEquals(1, $download_intent_count);

    // Another GET request to test view's cache invalidation.
    $this->getHttpClient()->request('GET', $url);
    $download_intent_count = $this->getDownloadIntentCountFromViewPage(1);
    $this->assertEquals(2, $download_intent_count);
  }

  /**
   * Test partial file request handling.
   */
  public function testResultsOnPartialFileRequests() {
    /** @var \SimpleXMLElement[] $channel */
    $channel = $this->feed->xpath('/rss/channel');
    $channel = reset($channel);

    // Episodes should be ordered by date - most recent first.
    $episode_three = $channel->item[0];

    $enclosure_attrs = $episode_three->enclosure->attributes();
    $url = (string) $enclosure_attrs->url;
    $this->assertNotEmpty($url);
    if ($base_url = getenv('SIMPLETEST_BASE_URL')) {
      $url = parse_url($url);
      $url = $base_url . $url['path'];
    }

    $avoffset = 184;
    $bitrate_in_bps = 151165.43836377;
    $filesize_in_bytes = 2000257;
    $one_minute_of_bytes = ceil($bitrate_in_bps / 8 * 60);

    // Test without header first.
    $this->getHttpClient()->request('GET', $url);
    $download_intent_count = $this->getDownloadIntentCountFromViewPage(3);
    $this->assertEquals(1, $download_intent_count);

    // Test one minute of bytes without offset.
    // This should not count because it does not consider the metadata offset.
    $this->getHttpClient()->request('GET', $url, $this->getHttprangeHeaderOptions(0, $one_minute_of_bytes));
    $download_intent_count = $this->getDownloadIntentCountFromViewPage(3);
    $this->assertEquals(1, $download_intent_count);

    // Test one minute of bytes with offset.
    // This should count because it does consider the metadata offset.
    $this->getHttpClient()->request('GET', $url, $this->getHttprangeHeaderOptions($avoffset, $one_minute_of_bytes + $avoffset));
    $download_intent_count = $this->getDownloadIntentCountFromViewPage(3);
    $this->assertEquals(2, $download_intent_count);

    // Count requests with byte range from 0-1.
    // @link https://www.drupal.org/project/podcast_publisher/issues/3428027
    $this->getHttpClient()->request('GET', $url, $this->getHttprangeHeaderOptions(0, 1));
    $download_intent_count = $this->getDownloadIntentCountFromViewPage(3);
    $this->assertEquals(3, $download_intent_count);
  }

  /**
   * Creates guzzle options for range headers.
   *
   * @param int $start
   *   Start of range.
   * @param int|null $end
   *   End of range.
   *
   * @return array[]
   *   The guzzle option array with set range header.
   */
  protected function getHttprangeHeaderOptions(int $start, ?int $end = NULL) {
    return [
      'headers' => [
        'Range' => "bytes=$start-$end",
      ],
    ];
  }

  /**
   * Gets the download intent count that's displayed on analytics view page.
   *
   * @param int $row
   *   The episode's row to check.
   *
   * @return int
   *   The download count as displayed on view page.
   */
  protected function getDownloadIntentCountFromViewPage(int $row): int {
    // Run queue first.
    $queue = $this->queue->get('podcast_download_intent_tracker');
    while ($item = $queue->claimItem()) {
      $this->downloadIntentTrackerQueueWorker->processItem($item->data);
    }
    // Get result.
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/content/podcast/' . $this->podcast->id() . '/analytics');
    $el = $page->find('xpath', "//div[contains(@class, 'view-podcast-analytics')]//tbody//tr[$row]/td[contains(@class, 'views-field-download-intent-count')]");
    $this->assertIsObject($el);
    return (int) $el->getText();
  }

}
