<?php

/**
 * @file
 * Module's views file.
 */

/**
 * Implements hook_views_data().
 */
function podcast_publisher_analytics_views_data() {
  $data = [];

  $data['podcast_download_intent'] = [];

  $data['podcast_download_intent']['table'] = [];

  $data['podcast_download_intent']['table']['group'] = t('Podcast Publisher Analytics');

  $data['podcast_download_intent']['table']['provider'] = 'podcast_publisher_analytics';

  $data['podcast_download_intent']['table']['base'] = [
    // Identifier (primary) field in this table for Views.
    'field' => 'id',
    // Label in the UI.
    'title' => t('Podcast Download Intent'),
    // Longer description in the UI. Required.
    'help' => t('Podcast Download Intent table contains podcast download entities.'),
    'weight' => -10,
  ];

  $data['podcast_download_intent']['table']['join'] = [
    'podcast_episode' => [
      'left_field' => 'id',
      'field' => 'episode',
    ],
  ];

  $data['podcast_episode']['download_intent_count'] = [
    'title' => t('Download Intent Count'),
    'help' => t('More information about the relationship'),
    'field' => [
      'id' => 'download_intent_count',
    ],
    'filter' => [
      'id' => 'download_intent_count_period',
      'label' => t('Download Intent Time Period'),
    ],
    'relationship' => [
      'base' => 'podcast_download_intent',
      'base field' => 'id',
      'field' => 'id',
      'id' => 'download_intent_count',
      'label' => t('Download Intent Count'),
    ],
  ];
  return $data;
}
