<?php

/**
 * @file
 * Install hooks for module.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\podcast_publisher_analytics\Plugin\QueueWorker\DownloadIntentTracker;

/**
 * Implements hook_requirements().
 */
function podcast_publisher_analytics_requirements($phase) {
  $requirements = [];

  // Geolite city database must be in place in order
  // for the module to function properly.
  if ($phase == 'install' && !is_file(\Drupal::root() . '/libraries/geolite2/GeoLite2-City.mmdb')) {
    $requirements = [
      'podcast_publisher_analytics.geolite2' => [
        'title' => t('Geolite2 City Database'),
        'value' => t('None'),
        'description' => t('GeoLite2 City Database must be downloaded to the libraries folder. Check out the documentation at https://podcastpublisher.org for more information!'),
        'severity' => REQUIREMENT_ERROR,
      ],
    ];
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function podcast_publisher_analytics_schema() {
  return [
    'podcast_download_intent' => [
      'description' => 'Download Intents',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'requester_id' => [
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 255,
          'description' => 'The ID of the requester.',
        ],
        'user_agent_string' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => 'The raw user agent string.',
        ],
        'user_agent' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'The referenced User Agent.',
        ],
        'source' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => "The request's source",
        ],
        'episode' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'ID of referenced podcast episode.',
        ],
        'file' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'ID of referenced audio file.',
        ],
        'geo_area' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => "ID of requester's geo area.",
        ],
        'lat' => [
          'type' => 'float',
          'description' => 'The latitude of the requester.',
        ],
        'lng' => [
          'type' => 'float',
          'description' => 'The longitude of the requester.',
        ],
        'timestamp' => [
          'type' => 'int',
          'description' => 'The timestamp of the file request.',
        ],
        'httprange' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => "HTTP Header's range of bytes that were requested.",
        ],
        'processed' => [
          'type' => 'int',
          'size' => 'tiny',
          'description' => 'Indicator whether requester ID was already processed to be anonymous.',
        ],
        'original_intent' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'This references the first call of given requester in given time frame.',
        ],
        'requested_bytes_total' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'Total amount of bytes requested in all related requests.',
        ],
        'count' => [
          'type' => 'int',
          'size' => 'tiny',
          'description' => 'Whether request should be counted.',
        ],
      ],
      'primary key' => ['id'],
      'foreign keys' => [
        'user_agent' => [
          'table' => 'podcast_user_agent',
          'columns' => [
            'id' => 'id',
          ],
        ],
        'episode' => [
          'table' => 'podcast_episode',
          'columns' => [
            'id' => 'id',
          ],
        ],
        'file' => [
          'table' => 'file_managed',
          'columns' => [
            'id' => 'id',
          ],
        ],
        'geo_area' => [
          'table' => 'podcast_geo_area',
          'columns' => [
            'id' => 'id',
          ],
        ],
        'original_intent' => [
          'table' => 'podcast_download_intent',
          'columns' => [
            'id' => 'id',
          ],
        ],
      ],
    ],
    'podcast_user_agent' => [
      'description' => 'User Agents',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'user_agent' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => 'The raw user agent string.',
        ],
        'bot' => [
          'type' => 'int',
          'size' => 'tiny',
          'description' => 'Indicator whether the user agent should be treated as a bot.',
        ],
        'client_name' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => 'The name of the client that requested the file.',
        ],
        'os_name' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => 'The name of the operating system the requester used.',
        ],
      ],
      'primary key' => ['id'],
    ],
    'podcast_geo_area' => [
      'description' => 'Geo Area',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'name' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => 'The name of the area.',
        ],
        'parent' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'Reference to the parent geo area.',
        ],
        'code' => [
          'type' => 'varchar',
          'length' => 20,
          'description' => "The area's code.",
        ],
        'area_type' => [
          'type' => 'varchar',
          'length' => 255,
          'description' => "The area's type.",
        ],
      ],
      'primary key' => ['id'],
      'foreign keys' => [
        'parent' => [
          'table' => 'podcast_geo_area',
          'columns' => [
            'id' => 'id',
          ],
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_install().
 */
function podcast_publisher_analytics_install() {
  // Create bitrate field.
  $bitrate_field = BaseFieldDefinition::create('float')
    ->setLabel(t('Bitrate'))
    ->setDescription(t('Bitrate of audio file in bits per second.'))
    ->setSettings([
      'size' => 'normal',
      'unsigned' => TRUE,
    ])
    ->setDisplayOptions('form', [
      'region' => 'hidden',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('bitrate', 'podcast_episode', 'podcast_publisher_analytics', $bitrate_field);

  // Create offset field.
  $avoffset_field = BaseFieldDefinition::create('integer')
    ->setLabel(t('Avoffset'))
    ->setDescription(t('Offset of audio file in bytes.'))
    ->setSettings([
      'size' => 'normal',
      'unsigned' => TRUE,
    ])
    ->setDisplayOptions('form', [
      'region' => 'hidden',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('avoffset', 'podcast_episode', 'podcast_publisher_analytics', $avoffset_field);
}

/**
 * Add new fields to episodes and schema.
 *
 * Implements hook_update_N().
 */
function podcast_publisher_analytics_update_9001(&$sandbox) {
  // First run install hook to add new fields to the podcast entity type.
  podcast_publisher_analytics_install();

  // Add fields to download intent table.
  $requested_bytes_total = [
    'type' => 'int',
    'unsigned' => TRUE,
    'description' => 'Total amount of bytes requested in all related requests.',
  ];
  $count = [
    'type' => 'int',
    'size' => 'tiny',
    'description' => 'Whether request should be counted.',
  ];
  $schema = Drupal::database()->schema();
  $schema->addField('podcast_download_intent', 'requested_bytes_total', $requested_bytes_total);
  $schema->addField('podcast_download_intent', 'count', $count);
}

/**
 * Count requests with byte range from 0-1.
 */
function podcast_publisher_analytics_update_9002(&$sandbox) {
  // Update all inital requests having a byte range 0-1.
  \Drupal::database()->update('podcast_download_intent')
    ->condition('httprange', 'bytes=0-1')
    ->condition('count', 0)
    ->fields(['count' => 1])
    ->execute();

  // Update initial requests of subsequent requests having a byte range 0-1.
  $stmt = \Drupal::database()->select('podcast_download_intent')
    ->fields('podcast_download_intent', ['requester_id'])
    ->condition('httprange', 'bytes=0-1')
    ->condition('count', NULL, 'IS NULL')
    ->groupBy('requester_id')
    ->execute();
  $requester_ids = array_keys($stmt->fetchAllAssoc('requester_id'));

  if ($requester_ids) {
    \Drupal::database()->update('podcast_download_intent')
      ->condition('requester_id', $requester_ids, 'IN')
      ->condition('count', 0)
      ->fields(['count' => 1])
      ->execute();
  }

}

/**
 * Create dummy bot user agent.
 */
function podcast_publisher_analytics_update_9003(&$sandbox) {
  $dummy_bot_id = \Drupal::database()->insert('podcast_user_agent')
    ->fields(DownloadIntentTracker::DUMMY_BOT_UA)
    ->execute();

  $query = \Drupal::database()->select('podcast_download_intent')
    ->fields(NULL, ['id']);
  $query->addExpression('LOWER(user_agent_string)', 'uastring');
  $result = $query->condition('user_agent', NULL, 'IS NULL')->having('uastring LIKE \'%bot%\'')
    ->execute();

  $ids = [];
  while ($row = $result->fetch()) {
    $ids[] = $row->id;
  }

  \Drupal::database()->update('podcast_download_intent')
    ->fields(['user_agent' => $dummy_bot_id])
    ->condition('id', $ids, 'IN')
    ->execute();
}
