<?php

namespace Drupal\podcast_publisher_analytics;

use Drupal\Component\Utility\UrlHelper;
use Drupal\file\FileInterface;
use Drupal\podcast_publisher\PodcastEpisodeInterface;

/**
 * Service to create tracking URL.
 */
class TrackingUrlGenerator {

  /**
   * Generates tracking URL.
   *
   * @param \Drupal\podcast_publisher\PodcastEpisodeInterface $episode
   *   The episode.
   * @param \Drupal\file\FileInterface $file
   *   The file.
   * @param string $source
   *   The source, e.g. 'feed'.
   *
   * @return string
   *   The tracking URL.
   */
  public function generateTrackingUrl(PodcastEpisodeInterface $episode, FileInterface $file, string $source = 'feed'): string {
    $file_url = $file->createFileUrl();

    if (UrlHelper::isExternal($file_url)) {
      $file_url_array = parse_url($file_url);
      $file_url_array['query'] = $file_url_array['query'] ?? '';
      $file_url_array['query'] .= $file_url_array['query'] ? '&' : '';
      $file_url_array['query'] .= 'external_host=' . urlencode($file_url_array['host']);
      $file_url = $file_url_array['path'] . '?' . $file_url_array['query'];
    }

    // @todo prefix must not be hard coded.
    return implode('/', [
      '/podcast-publisher',
      $source,
      $episode->id(),
      $file->id(),
      // Remove leading slash.
      ltrim($file_url, '/'),
    ]);
  }

}
