<?php

namespace Drupal\podcast_publisher_analytics;

use Drupal\Core\Database\Connection;
use Drupal\media\MediaInterface;

/**
 * Service to cope with download intents.
 */
class DownloadIntentDataService {

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Returns count of all downloads of given media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media to count downloads of.
   *
   * @return int
   *   The download count of given media.
   */
  public function getCountOfAllDownloads(MediaInterface $media): int {
    $query = $this->database->select('podcast_download_intent', 'pdi');
    $query->addField('pdi', 'requester_id', 'requester_id');
    $query->condition('media', $media->id());
    $query->groupBy('requester_id');
    return $query->countQuery()
      ->execute()
      ->fetchField() ?: 0;
  }

}
