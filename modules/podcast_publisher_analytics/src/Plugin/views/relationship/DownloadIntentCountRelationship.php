<?php

namespace Drupal\podcast_publisher_analytics\Plugin\views\relationship;

use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Relationship handler that creates subquery to count download intents.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("download_intent_count")
 */
class DownloadIntentCountRelationship extends RelationshipPluginBase {

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Join plugin manager.
   *
   * @var \Drupal\views\Plugin\views\join\JoinPluginInterface
   */
  protected $viewsJoinPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->database = $container->get('database');
    $instance->viewsJoinPluginManager = $container->get('plugin.manager.views.join');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    // Add a subquery to the query that will find the download count.
    $sub_query = $this->database->select('podcast_download_intent', 'pdi');
    $sub_query->leftJoin('podcast_user_agent', 'pua', 'pdi.user_agent = pua.id');
    $sub_query->fields('pdi', ['episode']);
    $sub_query->addExpression('COUNT(pdi.episode)', 'download_intent_count');
    $sub_query->isNull('pdi.original_intent');
    $bot_condition = $sub_query->orConditionGroup()->condition('pua.bot', 0)->isNull('pua.bot');
    $sub_query->condition($bot_condition);
    $sub_query->condition('pdi.count', 1);
    $sub_query->groupBy('pdi.episode');

    // Add the subquery as a component of a join.
    $joinDefinition = [
      'table formula' => $sub_query,
      'field' => 'episode',
      'left_table' => 'podcast_episode',
      'left_field' => 'id',
      'adjust' => TRUE,
    ];

    // Create a join object and create a relationship
    // between the main query and the subquery.
    $join = $this->viewsJoinPluginManager->createInstance('standard', $joinDefinition);
    $this->query->addRelationship('pdi', $join, 'podcast_download_intent');
  }

}
