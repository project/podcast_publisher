<?php

namespace Drupal\podcast_publisher_analytics\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter to handle periods to count download intents.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("download_intent_count_period")
 */
class DownloadIntentCountPeriod extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    if ($this->value == -1) {
      // Skip filter.
      return;
    }
    // Get DateTime of x days before today at midnight.
    $date = new \DateTime();

    if ($this->value != 1) {
      $date = $date->sub(new \DateInterval('P' . $this->value . 'D'));
    }
    $date->setTime(0, 0);

    /** @var \Drupal\Core\Database\Query\Select $subquery */
    $subquery = $this->query->getTableQueue()['pdi']['join']->configuration['table formula'];
    $subquery->condition('pdi.timestamp', $date->getTimestamp(), '>=');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['value'] = [
      'default' => -1,
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value'] = [
      '#type' => 'select',
      '#label' => $this->t('Download Intent Time Period'),
      '#options' => $this->getOptions(),
      '#default_value' => $this->value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function acceptExposedInput($input) {
    if (!parent::acceptExposedInput($input)) {
      return FALSE;
    }

    // Exposed form returns value as array.
    if (is_array($this->value)) {
      $this->value = reset($this->value);
    }

    return TRUE;
  }

  /**
   * Returns options of filter.
   *
   * @return array
   *   Associative array with period length in days as keys.
   */
  protected function getOptions() {
    return [
      -1 => $this->t('Since publication'),
      1 => $this->t('Today'),
      7 => $this->t('Last 7 days'),
      30 => $this->t('Last 30 days'),
      90 => $this->t('Last 90 days'),
    ];
  }

}
