<?php

namespace Drupal\podcast_publisher_analytics\Plugin\views\field;

use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;

/**
 * Field handler to show number of download intents.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("download_intent_count")
 */
class DownloadIntentCountField extends NumericField {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    // Add the field to the Views interface.
    $this->field_alias = $this->query->addField(NULL, 'download_intent_count', 'download_intent_count');
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = parent::render($values);
    return [
      '#markup' => $value,
      '#cache' => ['tags' => ['podcast_download_intent']],
    ];
  }

}
