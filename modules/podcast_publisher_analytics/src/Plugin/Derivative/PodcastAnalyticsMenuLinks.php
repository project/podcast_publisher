<?php

namespace Drupal\podcast_publisher_analytics\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create podcast related menu links in admin menu.
 */
class PodcastAnalyticsMenuLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The Podcast entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $podcastStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new self();
    $instance->podcastStorage = $container->get('entity_type.manager')
      ->getStorage('podcast');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    /** @var \Drupal\podcast_publisher\PodcastInterface[] $podcasts */
    $podcasts = $this->podcastStorage->loadMultiple();

    foreach ($podcasts as $podcast) {
      $key_view = 'entity.podcast.view.' . $podcast->id();

      $key_episode = 'entity.podcast_episode.analytics.' . $podcast->id();
      $links[$key_episode] = [
        'title' => $this->t('Analytics (Alpha!)'),
        'route_name' => 'view.podcast_analytics.page',
        'parent' => 'podcast_publisher.extra_links:' . $key_view,
        'route_parameters' => ['podcast' => $podcast->id()],
        'weight' => 10,
      ] + $base_plugin_definition;

    }
    return $links;
  }

}
