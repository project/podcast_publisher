<?php

namespace Drupal\podcast_publisher_analytics\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create podcast related menu links in admin menu.
 */
class PodcastAnalyticsLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The Podcast entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $podcastStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new self();
    $instance->podcastStorage = $container->get('entity_type.manager')
      ->getStorage('podcast');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    /** @var \Drupal\podcast_publisher\PodcastInterface[] $podcasts */
    $podcasts = $this->podcastStorage->loadMultiple();

    foreach ($podcasts as $podcast) {
      $key_view = 'podcast_publisher_analytics.analytics_view.' . $podcast->id();

      $links[$key_view] = [
        'title' => $this->t('Analytics'),
        'route_name' => 'view.podcast_analytics.page',
        'base_route' => 'entity.podcast.canonical',
        'route_parameters' => ['podcast' => $podcast->id()],
        'weight' => 100,
      ] + $base_plugin_definition;
    }

    return $links;
  }

}
