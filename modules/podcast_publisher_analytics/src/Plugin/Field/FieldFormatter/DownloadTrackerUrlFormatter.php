<?php

namespace Drupal\podcast_publisher_analytics\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "podcast_publisher_download_tracker",
 *   label = @Translation("Download Tracker URL"),
 *   description = @Translation("Create tracking URL for podcast episodes"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class DownloadTrackerUrlFormatter extends EntityReferenceFormatterBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The tracking URL generator.
   *
   * @var \Drupal\podcast_publisher_analytics\TrackingUrlGenerator
   */
  protected $trackingUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->trackingUrlGenerator = $container->get('podcast_publisher_analytics.tracking_url_generator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    /** @var \Drupal\podcast_publisher\PodcastEpisodeInterface $episode */
    $episode = $items->getEntity();

    /** @var \Drupal\file\FileInterface $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $url = $this->trackingUrlGenerator
        ->generateTrackingUrl($episode, $file, $this->getSetting('source'));

      // Render only URL.
      $elements[$delta] = [
        '#plain_text' => $url,
        '#cache' => [
          'tags' => array_merge($episode->getCacheTags(), $file->getCacheTags()),
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that reference
    // podcast episodes.
    return $field_definition->getTargetEntityTypeId() == 'podcast_episode';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'source' => 'feed',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source'),
      '#description' => $this->t("String to identify the user's source, e.g. 'webplayer' or 'feed'"),
      '#default_value' => $this->getSetting('source'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Source: %source', ['%source' => $this->getSetting('source')]);
    return $summary;
  }

}
