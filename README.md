# Podcast Publisher

Podcast Publisher is a module to easily create your own podcast feed.

The goal is to have a simple plug and play solution for site builders that's 
easily configurable and fits their needs of publishing podcasts.

It provides a new content type Podcast, a new media type Podcast Episode
and a view to render the feed.

## Development

If you want to see the development progress check out our [Trello boards](https://trello.com/w/podcastpublisher)

## Documentation

The documentation can be found [here](https://podcastpublisher.org)

## Alternatives

The [Podcast (Using Views)](https://www.drupal.org/project/podcast) module provides a more flexible solution, if you already have a data model.

## Maintainers

* [Arthur Lorenz](https://drupal.org/u/arthur_lorenz)
* [Norman Kämper-Leymann](https://drupal.org/u/leymannx)

## Credits

A big ❤️️ goes out to the [Podlove](https://podlove.org/) project which was one of the main sources of inspiration during development.
