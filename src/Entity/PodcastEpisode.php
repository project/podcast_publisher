<?php

namespace Drupal\podcast_publisher\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\podcast_publisher\PodcastEpisodeInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the podcast episode entity class.
 *
 * @ContentEntityType(
 *   id = "podcast_episode",
 *   label = @Translation("Podcast Episode"),
 *   label_collection = @Translation("Podcast Episodes"),
 *   label_singular = @Translation("podcast episode"),
 *   label_plural = @Translation("podcast episodes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count podcast episodes",
 *     plural = "@count podcast episodes",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\podcast_publisher\PodcastEpisodeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\podcast_publisher\PodcastEpisodeAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\podcast_publisher\Form\PodcastEpisodeForm",
 *       "edit" = "Drupal\podcast_publisher\Form\PodcastEpisodeForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "podcast_episode",
 *   revision_table = "podcast_episode_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer podcast episode",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/podcast/{podcast}/episodes",
 *     "add-form" = "/podcast-episode/add",
 *     "canonical" = "/podcast-episode/{podcast_episode}",
 *     "edit-form" = "/podcast-episode/{podcast_episode}/edit",
 *     "delete-form" = "/podcast-episode/{podcast_episode}/delete",
 *   },
 *   field_ui_base_route = "entity.podcast_episode.settings",
 * )
 */
class PodcastEpisode extends RevisionableContentEntityBase implements PodcastEpisodeInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
    // Generate audio duration value.
    $file = $this->get('audio_file')->entity;
    if (!$file) {
      return;
    }
    $id3 = new \getID3();
    $fileinfo = $id3->analyze($file->getFileUri());
    $this->set('duration', $fileinfo['playtime_string'] ?? '0');

    // Fill fields if created by podcast_publisher_analytics module.
    if ($this->hasField('bitrate')) {
      $this->set('bitrate', $fileinfo['bitrate'] ?? 0);
    }
    if ($this->hasField('avoffset')) {
      $this->set('avoffset', $fileinfo['avdataoffset'] ?? 0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the podcast episode was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the podcast episode was last edited.'));

    $fields['duration'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Duration'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['episode_image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setDescription(t("The podcast episode's image."))
      ->setTargetEntityTypeId('image')
      ->setSettings([
        'alt_field' => TRUE,
        'alt_field_required' => TRUE,
        'title_field' => TRUE,
        'title_field_required' => FALSE,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['episode_number'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Episode Number'))
      ->setSettings([
        'size' => 'normal',
        'unsigned' => TRUE,
      ])
      ->setDisplayOptions('form', [
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['explicit'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Explicit'))
      ->setDescription(t('Whether the podcast is explicit.'))
      ->setDisplayOptions('form', [
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['podcast'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel('Podcast')
      ->setRequired(TRUE)
      ->setDescription(t('The podcast this episode belongs to.'))
      ->setSetting('target_type', 'podcast')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['audio_file'] = BaseFieldDefinition::create('file')
      ->setLabel('Audio File')
      ->setRequired(TRUE)
      ->setDescription(t("The episode's audio file."))
      ->setSetting('target_type', 'file')
      ->setSetting('file_directory', 'podcast/[date:custom:Y]/[date:custom:m]')
      ->setSetting('file_extensions', 'mp3 wav aac')
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['shownotes'] = BaseFieldDefinition::create('text_long')
      ->setLabel('Shownotes')
      ->setDescription(t('Add a summary and additional information for the listener.'))
      ->setSetting('target_type', 'file')
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Type'))
      ->setDescription(t('Type of Episode.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDefaultValue('full')
      ->setSetting('allowed_values', [
        'full' => 'Full',
        'trailer' => 'Trailer',
        'bonus' => 'Bonus',
      ])
      ->setDisplayOptions('form', [
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['authors'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('The user ID of the podcast episode author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['keywords'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Keywords'))
      ->setName('keywords')
      ->setTargetEntityTypeId('podcast_episode')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Keywords categorizing the podcast episode. Multiple entries are separated by a comma.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['tags' => 'tags'],
        'auto_create' => TRUE,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete_tags',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'label',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
