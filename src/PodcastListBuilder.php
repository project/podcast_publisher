<?php

namespace Drupal\podcast_publisher;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the podcast entity type.
 */
class PodcastListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = new self(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id())
    );
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->redirectDestination = $container->get('redirect.destination');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(TRUE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total podcasts: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');
    $header['status'] = $this->t('Status');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    $header['links'] = $this->t('Links');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $podcast) {
    $row['id'] = $podcast->id();
    $row['title'] = $podcast->toLink();
    $row['status'] = $podcast->isEnabled() ? $this->t('Enabled') : $this->t('Disabled');
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $podcast->getOwner(),
    ];
    $row['created'] = $this->dateFormatter->format($podcast->getCreatedTime());
    $row['changed'] = $this->dateFormatter->format($podcast->getChangedTime());
    $row['links']['data'] = [
      '#type' => 'operations',
      '#links' => $this->getLinks($podcast),
    ];
    return $row + parent::buildRow($podcast);
  }

  /**
   * Returns the links to episodes and analytics.
   *
   * @param \Drupal\Core\Entity\EntityInterface $podcast
   *   The podacast.
   *
   * @return array[]
   *   The links.
   */
  protected function getLinks(EntityInterface $podcast): array {
    $links = [
      'episodes' => [
        'title' => $this->t('Episodes'),
        'weight' => 10,
        'url' => Url::fromRoute(
          'entity.podcast_episode.collection',
          ['podcast' => $podcast->id()],
        ),
      ],
    ];

    if ($this->moduleHandler()->moduleExists('podcast_publisher_analytics')) {
      $links['analytics'] = [
        'title' => $this->t('Analytics'),
        'weight' => 20,
        'url' => Url::fromRoute(
          'view.podcast_analytics.page',
          ['podcast' => $podcast->id()],
        ),
      ];
    }

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}
