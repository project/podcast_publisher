<?php

namespace Drupal\podcast_publisher;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the podcast episode entity type.
 */
class PodcastEpisodeListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Podcast entity.
   *
   * @var \Drupal\podcast_publisher\PodcastInterface
   */
  protected $podcast;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = new self(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
    );
    $instance->dateFormatter = $container->get('date.formatter');
    $podcast_id = $container->get('current_route_match')
      ->getParameter('podcast');
    $instance->podcast = $container->get('entity_type.manager')
      ->getStorage('podcast')
      ->load($podcast_id);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    $build['#title'] = $this->t('Episodes of "@podcast" Podcast', ['@podcast' => $this->podcast->label()]);

    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->condition('podcast', $this->podcast->id())
      ->accessCheck(TRUE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total episodes for "@podcast": @total', [
      '@total' => $total,
      '@podcast' => $this->podcast->label(),
    ]);
    return $build;
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->condition('podcast', $this->podcast->id())
      ->accessCheck(TRUE)
      ->sort('created', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['status'] = $this->t('Status');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $podcast_episode) {
    /** @var \Drupal\podcast_publisher\PodcastEpisodeInterface $podcast_episode */
    $row['id'] = $podcast_episode->id();
    $row['label'] = $podcast_episode->toLink();
    $row['status'] = $podcast_episode->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $podcast_episode->getOwner(),
    ];
    $row['created'] = $this->dateFormatter->format($podcast_episode->get('created')->value);
    $row['changed'] = $this->dateFormatter->format($podcast_episode->getChangedTime());
    return $row + parent::buildRow($podcast_episode);
  }

}
