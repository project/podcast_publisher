<?php

namespace Drupal\podcast_publisher;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a podcast episode entity type.
 */
interface PodcastEpisodeInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
