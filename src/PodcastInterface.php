<?php

namespace Drupal\podcast_publisher;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a podcast entity type.
 */
interface PodcastInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the podcast title.
   *
   * @return string
   *   Title of the podcast.
   */
  public function getTitle();

  /**
   * Sets the podcast title.
   *
   * @param string $title
   *   The podcast title.
   *
   * @return \Drupal\podcast_publisher\PodcastInterface
   *   The called podcast entity.
   */
  public function setTitle($title);

  /**
   * Gets the podcast creation timestamp.
   *
   * @return int
   *   Creation timestamp of the podcast.
   */
  public function getCreatedTime();

  /**
   * Sets the podcast creation timestamp.
   *
   * @param int $timestamp
   *   The podcast creation timestamp.
   *
   * @return \Drupal\podcast_publisher\PodcastInterface
   *   The called podcast entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the podcast status.
   *
   * @return bool
   *   TRUE if the podcast is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the podcast status.
   *
   * @param bool $status
   *   TRUE to enable this podcast, FALSE to disable.
   *
   * @return \Drupal\podcast_publisher\PodcastInterface
   *   The called podcast entity.
   */
  public function setStatus($status);

}
