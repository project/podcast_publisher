<?php

namespace Drupal\podcast_publisher;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service to generate absolute URLs.
 */
class AbsoluteUrlGenerator {

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Create absolute url generator service.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * Generates absolute URLs from path.
   *
   * @param string $path
   *   The URL's path.
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The request to generate the url from, defaults to current request.
   *
   * @return string
   *   The absolute URL.
   */
  public function generateAbsoluteUrl(string $path, ?Request $request = NULL): string {
    // If the given URL already starts with a leading slash, it's been processed
    // and we need to simply make it an absolute path by prepending the host.
    if (str_starts_with($path, '/') && !str_starts_with($path, '//')) {
      if (!$request) {
        $request = $this->requestStack->getCurrentRequest();
      }
      $host = $request->getSchemeAndHttpHost();
      // @todo Views should expect and store a leading /.
      // @see https://www.drupal.org/node/2423913
      return $host . $path;
    }
    elseif (UrlHelper::isValid($path)) {
      if (UrlHelper::isExternal($path)) {
        return $path;
      }
      return Url::fromUserInput($path)->setAbsolute()->toString();
    }
    // Otherwise, this is an unprocessed path (e.g. node/123) and we need to run
    // it through a Url object to allow outbound path processors to run (path
    // aliases, language prefixes, etc).
    else {
      return Url::fromUserInput('/' . $path)->setAbsolute()->toString();
    }
  }

}
