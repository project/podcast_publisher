<?php

namespace Drupal\podcast_publisher\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "podcast_publisher_mimetype",
 *   label = @Translation("Mimetype"),
 *   description = @Translation("Display the Mimetype of the referenced audio file."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileMimeType extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      if ($file->id()) {
        $elements[$delta] = [
          '#plain_text' => $file->getMimeType(),
          // Create a cache tag entry for the referenced entity. In the case
          // that the referenced entity is deleted, the cache for referring
          // entities must be cleared.
          '#cache' => [
            'tags' => $file->getCacheTags(),
          ],
        ];
      }
    }

    return $elements;
  }

}
