<?php

namespace Drupal\podcast_publisher\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "podcast_publisher_filelength",
 *   label = @Translation("File Length"),
 *   description = @Translation("Display the File Length of the referenced
 *   audio file."), field_types = {
 *     "file"
 *   }
 * )
 */
class FileLength extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\file\Entity\File $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      if ($file->id()) {
        $elements[$delta] = [
          '#plain_text' => $file->getSize(),
          // Create a cache tag entry for the referenced entity. In the case
          // that the referenced entity is deleted, the cache for referring
          // entities must be cleared.
          '#cache' => [
            'tags' => $file->getCacheTags(),
          ],
        ];
      }
    }

    return $elements;
  }

}
