<?php

namespace Drupal\podcast_publisher\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\podcast_publisher\PodcastInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates podcast episode action links.
 */
class PodcastEpisodeActionLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new self();
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links['podcast_episode.add_form'] = $base_plugin_definition;
    $links['podcast_episode.add_form']['title'] = 'Add podcast episode';
    $links['podcast_episode.add_form']['route_name'] = 'entity.podcast_episode.add_form';
    $links['podcast_episode.add_form']['appears_on'] = ['entity.podcast_episode.collection'];
    if ($podcast = $this->routeMatch->getParameter('podcast')) {
      if ($podcast instanceof PodcastInterface) {
        $podcast = $podcast->id();
      }
      $links['podcast_episode.add_form']['route_parameters'] = ['podcast' => $podcast];
    }
    return $links;
  }

}
