<?php

namespace Drupal\podcast_publisher\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create podcast related menu links in admin menu.
 */
class PodcastEpisodeMenuLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The Podcast entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $podcastStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new self();
    $instance->podcastStorage = $container->get('entity_type.manager')
      ->getStorage('podcast');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    /** @var \Drupal\podcast_publisher\PodcastInterface[] $podcasts */
    $podcasts = $this->podcastStorage->loadMultiple();

    foreach ($podcasts as $podcast) {
      $key_view = 'entity.podcast.view.' . $podcast->id();
      $links[$key_view] = [
        'title' => $podcast->label(),
        'route_name' => 'entity.podcast.canonical',
        'parent' => 'podcast_publisher.content',
        'route_parameters' => ['podcast' => $podcast->id()],
      ] + $base_plugin_definition;

      $key_edit = 'entity.podcast.edit_form.' . $podcast->id();
      $links[$key_edit] = [
        'title' => $this->t('Edit'),
        'route_name' => 'entity.podcast.edit_form',
        'parent' => $base_plugin_definition['id'] . ':' . $key_view,
        'route_parameters' => ['podcast' => $podcast->id()],
      ] + $base_plugin_definition;

      $key_episode = 'entity.podcast_episode.collection.' . $podcast->id();
      $links[$key_episode] = [
        'title' => $this->t('Episode List'),
        'route_name' => 'entity.podcast_episode.collection',
        'parent' => $base_plugin_definition['id'] . ':' . $key_view,
        'route_parameters' => ['podcast' => $podcast->id()],
      ] + $base_plugin_definition;

    }
    return $links;
  }

}
