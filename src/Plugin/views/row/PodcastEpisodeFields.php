<?php

namespace Drupal\podcast_publisher\Plugin\views\row;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\row\RowPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Renders an RSS item based on fields.
 *
 * @ViewsRow(
 *   id = "podcast_episode_fields",
 *   title = @Translation("Podcast Episode Fields"),
 *   help = @Translation("Display fields as RSS items."),
 *   theme = "views_view_row_rss",
 *   display_types = {"feed"}
 * )
 */
class PodcastEpisodeFields extends RowPluginBase {

  /**
   * Does the row plugin support to add fields to its output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The elements.
   *
   * @var array
   */
  protected $elements = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['title_field'] = ['default' => ''];
    $options['link_field'] = ['default' => ''];
    $options['description_field'] = ['default' => ''];
    $options['file_field'] = ['default' => ''];
    $options['mimetype_field'] = ['default' => ''];
    $options['duration_field'] = ['default' => ''];
    $options['date_field'] = ['default' => ''];
    $options['type_field'] = ['default' => ''];
    $options['author_field'] = ['default' => ''];
    $options['image_field'] = ['default' => ''];
    $options['explicit_field'] = ['default' => ''];
    $options['keywords_field'] = ['default' => ''];

    $options['guid_field_options']['contains']['guid_field'] = ['default' => ''];
    $options['guid_field_options']['contains']['guid_field_is_permalink'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $initial_labels = ['' => $this->t('- None -')];
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    $form['title_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Title field'),
      '#description' => $this->t('The field that is going to be used as the RSS item title for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['title_field'],
      '#required' => TRUE,
    ];
    $form['link_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Link field'),
      '#description' => $this->t('The field that is going to be used as the RSS item link for each row. This must either be an internal unprocessed path like "node/123" or a processed, root-relative URL as produced by fields like "Link to content".'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['link_field'],
      '#required' => TRUE,
    ];
    $form['description_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Description field'),
      '#description' => $this->t('The field that is going to be used as the RSS item description for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['description_field'],
      '#required' => TRUE,
    ];
    $form['file_field'] = [
      '#type' => 'select',
      '#title' => $this->t('File field'),
      '#description' => $this->t('The field that is going to be used as the RSS item enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['file_field'],
      '#required' => TRUE,
    ];
    $form['file_size_field'] = [
      '#type' => 'select',
      '#title' => $this->t('File size field'),
      '#description' => $this->t('The field that is going to be used as the RSS item enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['file_size_field'],
      '#required' => TRUE,
    ];
    $form['mimetype_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Mimetype field'),
      '#description' => $this->t('The field that is going to be used as the RSS item enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['mimetype_field'],
      '#required' => TRUE,
    ];
    $form['duration_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Duration field'),
      '#description' => $this->t('The field that is going to be used as the RSS item enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['duration_field'],
      '#required' => TRUE,
    ];
    $form['image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#description' => $this->t('The field that is going to be used as the RSS itunes:image enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['image_field'],
    ];
    $form['type_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Type field'),
      '#description' => $this->t('The field that is going to be used as the RSS itunes:episodeType enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['type_field'],
    ];
    $form['author_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Author field'),
      '#description' => $this->t('The field that is going to be used as the RSS itunes:author enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['author_field'],
    ];
    $form['explicit_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Explicit field'),
      '#description' => $this->t('The field that is going to be used as the RSS itunes:explicit enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['explicit_field'],
    ];
    $form['keywords_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Keywords field'),
      '#description' => $this->t('The field that is going to be used as the RSS itunes:keywords enclosure for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['keywords_field'],
    ];
    $form['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Publication date field'),
      '#description' => $this->t('The field that is going to be used as the RSS item pubDate for each row. It needs to be in RFC 2822 format.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['date_field'],
      '#required' => TRUE,
    ];
    $form['episode_number_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Episode number field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:episode for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['episode_number_field'],
      '#required' => TRUE,
    ];
    $form['guid_field_options'] = [
      '#type' => 'details',
      '#title' => $this->t('GUID settings'),
      '#open' => TRUE,
    ];
    $form['guid_field_options']['guid_field'] = [
      '#type' => 'select',
      '#title' => $this->t('GUID field'),
      '#description' => $this->t('The globally unique identifier of the RSS item.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['guid_field_options']['guid_field'],
      '#required' => TRUE,
    ];
    $form['guid_field_options']['guid_field_is_permalink'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('GUID is permalink'),
      '#description' => $this->t('The RSS item GUID is a permalink.'),
      '#default_value' => $this->options['guid_field_options']['guid_field_is_permalink'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    $required_options = [
      'title_field',
      'link_field',
      'description_field',
      'date_field',
      'file_field',
    ];
    foreach ($required_options as $required_option) {
      if (empty($this->options[$required_option])) {
        $errors[] = $this->t('Row style plugin requires specifying which views fields to use for RSS item.');
        break;
      }
    }
    // Once more for guid.
    if (empty($this->options['guid_field_options']['guid_field'])) {
      $errors[] = $this->t('Row style plugin requires specifying which views fields to use for RSS item.');
    }
    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    static $row_index;
    if (!isset($row_index)) {
      $row_index = 0;
    }
    if (function_exists('rdf_get_namespaces')) {
      // Merge RDF namespaces in the XML namespaces in case they are used
      // further in the RSS content.
      $xml_rdf_namespaces = [];
      foreach (rdf_get_namespaces() as $prefix => $uri) {
        $xml_rdf_namespaces['xmlns:' . $prefix] = $uri;
      }
      $this->view->style_plugin->namespaces += $xml_rdf_namespaces;
    }

    // Create the RSS item object.
    $item = new \stdClass();
    $item->title = $this->getField($row_index, $this->options['title_field']);
    $item->link = $this->getAbsoluteUrl($this->getField($row_index, $this->options['link_field']));

    $field = $this->getField($row_index, $this->options['description_field']);
    $item->description = is_array($field) ? $field : ['#markup' => $field];

    $item->elements = $this->getElements($row_index);

    $this->elements[] = [
      'key' => 'content:encoded',
      'value' => $item->description,
    ];

    $row_index++;

    foreach ($item->elements as $element) {
      if (isset($element['namespace'])) {
        $this->view->style_plugin->namespaces = array_merge($this->view->style_plugin->namespaces, $element['namespace']);
      }
    }

    $build = [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#row' => $item,
      '#field_alias' => $this->field_alias ?? '',
    ];

    return $build;
  }

  /**
   * Retrieves a views field value from the style plugin.
   *
   * @param int $index
   *   The index count of the row as expected by views_plugin_style::getField().
   * @param string $field_id
   *   The ID assigned to the required field in the display.
   *
   * @return string|null|\Drupal\Component\Render\MarkupInterface
   *   An empty string if there is no style plugin, or the field ID is empty.
   *   NULL if the field value is empty. If neither of these conditions apply,
   *   a MarkupInterface object containing the rendered field value.
   */
  public function getField($index, $field_id) {
    if (empty($this->view->style_plugin) || !is_object($this->view->style_plugin) || empty($field_id)) {
      return '';
    }
    return $this->view->style_plugin->getField($index, $field_id);
  }

  /**
   * Gets RSS elements.
   *
   * @param int $row_index
   *   The row index.
   *
   * @return array
   *   The elements.
   */
  protected function getElements($row_index) {
    if ($file_value = $this->getField($row_index, $this->options['file_field'])) {
      $length = $this->getField($row_index, $this->options['file_size_field']);
      $mimetype = $this->getField($row_index, $this->options['mimetype_field']);
      $elements[] = [
        'key' => 'enclosure',
        'attributes' => [
          'url' => $this->getAbsoluteUrl($file_value),
          'type' => $mimetype,
          'length' => $length,
        ],
      ];
    }
    if ($image_value = $this->getField($row_index, $this->options['image_field'])) {
      $elements[] = [
        'key' => 'itunes:image',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
        'attributes' => [
          'href' => $this->getAbsoluteUrl($image_value),
        ],
      ];
    }

    $element_definitions = [
      [
        'key' => 'pubDate',
        'option_field' => 'date_field',
      ],
      [
        'key' => 'itunes:duration',
        'option_field' => 'duration_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:title',
        'option_field' => 'title_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:episode',
        'option_field' => 'episode_number_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:explicit',
        'option_field' => 'explicit_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:author',
        'option_field' => 'author_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:episodeType',
        'option_field' => 'type_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:keywords',
        'option_field' => 'keywords_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
    ];
    foreach ($element_definitions as $element_definition) {
      $value = $this->getField($row_index, $this->options[$element_definition['option_field']]);
      if ($value) {
        $element_definition['value'] = $value;
        unset($element_definition['option_field']);
        $elements[] = $element_definition;
      }
    }
    $guid_is_permalink_string = 'false';
    $item_guid = $this->getField($row_index, $this->options['guid_field_options']['guid_field']);
    if ($this->options['guid_field_options']['guid_field_is_permalink']) {
      $guid_is_permalink_string = 'true';
    }
    $item_guid = $this->getAbsoluteUrl($item_guid);
    $elements[] = [
      'key' => 'guid',
      'value' => $item_guid,
      'attributes' => ['isPermaLink' => $guid_is_permalink_string],
    ];
    return $elements;
  }

  /**
   * Convert a rendered URL string to an absolute URL.
   *
   * @param string $url_string
   *   The rendered field value ready for display in a normal view.
   *
   * @return string
   *   A string with an absolute URL.
   */
  protected function getAbsoluteUrl($url_string) {
    // If the given URL already starts with a leading slash, it's been processed
    // and we need to simply make it an absolute path by prepending the host.
    if (strpos($url_string, '/') === 0) {
      $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
      // @todo Views should expect and store a leading /.
      // @see https://www.drupal.org/node/2423913
      return $host . $url_string;
    }
    elseif (UrlHelper::isValid($url_string)) {
      if (preg_match('/^http(s?):\/\//', $url_string)) {
        return $url_string;
      }
      return Url::fromUserInput($url_string)->setAbsolute()->toString();
    }
    // Otherwise, this is an unprocessed path (e.g. node/123) and we need to run
    // it through a Url object to allow outbound path processors to run (path
    // aliases, language prefixes, etc).
    else {
      return Url::fromUserInput('/' . $url_string)->setAbsolute()->toString();
    }
  }

}
