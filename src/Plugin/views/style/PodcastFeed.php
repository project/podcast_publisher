<?php

namespace Drupal\podcast_publisher\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default style plugin to render an RSS feed.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "podcast",
 *   title = @Translation("Podcast RSS Feed"),
 *   help = @Translation("Generates an RSS feed from a view."),
 *   theme = "views_view_podcast_rss",
 *   display_types = {"feed"}
 * )
 */
class PodcastFeed extends StylePluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Absolute url generator.
   *
   * @var \Drupal\podcast_publisher\AbsoluteUrlGenerator
   */
  protected $absoluteUrlGenerator;

  /**
   * The namespaces.
   *
   * @var array
   */
  public $namespaces = [];

  /**
   * The channel elements.
   *
   * @var array
   */
  public $channel_elements = []; // phpcs:ignore

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->absoluteUrlGenerator = $container->get('podcast_publisher.absolute_url_generator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['title_field'] = ['default' => ''];
    $options['subtitle_field'] = ['default' => ''];
    $options['description_field'] = ['default' => ''];
    $options['creator_field'] = ['default' => ''];
    $options['category_field'] = ['default' => []];
    $options['itunes_category_field'] = ['default' => []];
    $options['copyright_field'] = ['default' => ''];
    $options['complete_field'] = ['default' => ''];
    $options['summary_field'] = ['default' => ''];
    $options['keywords_field'] = ['default' => ''];
    $options['image_field'] = ['default' => []];
    $options['owner_field'] = ['default' => ''];
    $options['email_field'] = ['default' => ''];
    $options['author_field'] = ['default' => ''];
    $options['type_field'] = ['default' => ''];
    $options['block_field'] = ['default' => ''];
    $options['explicit_field'] = ['default' => ''];
    $options['funding_field'] = ['default' => ''];

    $options['date_field'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $initial_labels = ['' => $this->t('- None -')];
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    $form['title_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Title field'),
      '#description' => $this->t('The field that is going to be used as the RSS item title for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['title_field'],
      '#required' => TRUE,
    ];
    $form['subtitle_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Subtitle field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:subtitle for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['subtitle_field'],
    ];
    $form['description_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Description field'),
      '#description' => $this->t('The field that is going to be used as the RSS item description for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['description_field'],
      '#required' => TRUE,
    ];
    $form['creator_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Creator field'),
      '#description' => $this->t('The field that is going to be used as the RSS item creator for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['creator_field'],
      '#required' => TRUE,
    ];
    $form['image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#description' => $this->t('The field that is going to be used as the RSS item image for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['image_field'],
      '#required' => TRUE,
    ];
    $form['copyright_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Copyright field'),
      '#description' => $this->t('The field that is going to be used as the RSS item copyright for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['copyright_field'],
    ];
    $form['summary_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Summary field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:summary for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['summary_field'],
    ];
    $form['type_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Type field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:type for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['type_field'],
    ];
    $form['owner_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Owner field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:owner for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['owner_field'],
    ];
    $form['email_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Email field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:email for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['email_field'],
    ];
    $form['author_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Author field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:author for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['author_field'],
    ];
    $form['category_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Category field'),
      '#description' => $this->t('The field that is going to be used as the RSS item category for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['creator_field'],
    ];
    $form['itunes_category_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Itunes Category field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:category for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes_category_field'],
    ];
    $form['block_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Block field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:block for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['block_field'],
    ];
    $form['explicit_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Explicit field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:explicit for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['explicit_field'],
    ];
    $form['complete_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Complete field'),
      '#description' => $this->t('The field that is going to be used as the RSS item itunes:complete for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['complete_field'],
    ];
    $form['funding_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Funding field'),
      '#description' => $this->t('The field that is going to be used as the RSS item podcast:funding for the channel.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['funding_field'],
    ];
    $form['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Publication date field'),
      '#description' => $this->t('The field that is going to be used as the RSS item pubDate for the channel. It needs to be in RFC 2822 format.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['date_field'],
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    if (empty($this->view->rowPlugin)) {
      trigger_error('Drupal\views\Plugin\views\style\Rss: Missing row plugin', E_WARNING);
      return [];
    }
    $rows = [];

    // This will be filled in by the row plugin and is used later on in the
    // theming output.
    $this->namespaces = ['xmlns:dc' => 'http://purl.org/dc/elements/1.1/'];

    // Fetch any additional elements for the channel and merge in their
    // namespaces.
    $this->channel_elements = $this->getChannelElements();
    foreach ($this->channel_elements as $element) {
      if (isset($element['namespace'])) {
        $this->namespaces = array_merge($this->namespaces, $element['namespace']);
      }
    }

    foreach ($this->view->result as $row_index => $row) {
      $this->view->row_index = $row_index;
      $rows[] = $this->view->rowPlugin->render($row);
    }

    $build = [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#rows' => $rows,
      '#attached' => [
        'http_header' => [
          ['Content-Type', 'application/rss+xml; charset=utf-8'],
        ],
      ],
    ];
    unset($this->view->row_index);
    return $build;
  }

  /**
   * Gets the channel's elements.
   *
   * @return array
   *   The channel elements.
   */
  protected function getChannelElements() {
    $elements = [];
    $element_definitions = [
      [
        'key' => 'pubDate',
        'option_field' => 'date_field',
      ],
      [
        'key' => 'copyright',
        'option_field' => 'copyright_field',
      ],
      [
        'key' => 'itunes:author',
        'option_field' => 'author_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:summary',
        'option_field' => 'summary_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:type',
        'option_field' => 'type_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:subtitle',
        'option_field' => 'subtitle_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:block',
        'option_field' => 'block_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'itunes:complete',
        'option_field' => 'complete_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
      [
        'key' => 'podcast:funding',
        'option_field' => 'funding_field',
        'namespace' => ['xmlns:podcast' => 'https://podcastindex.org/namespace/1.0'],
      ],
      [
        'key' => 'itunes:explicit',
        'option_field' => 'explicit_field',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      ],
    ];
    foreach ($element_definitions as $element_definition) {
      $value = $this->getField(0, $this->options[$element_definition['option_field']]);
      if ($value) {
        $element_definition['value'] = $value;
        unset($element_definition['option_field']);
        $elements[] = $element_definition;
      }
    }
    if ($value = $this->getField(0, $this->options['category_field'])) {
      $categories = explode(',', $value);
      foreach ($categories as $category) {
        $elements[] = [
          'key' => 'category',
          'value' => $category,
        ];
      }
    }
    if ($value = $this->getField(0, $this->options['itunes_category_field'])) {
      $elements = array_merge($elements, $this->getItunesCategories($value));
    }
    if ($this->getField(0, $this->options['owner_field']) || $this->getField(0, $this->options['email_field'])) {
      $elements[] = $this->getOwnerItem();
    }

    if ($image_value = $this->getField(0, $this->options['image_field'])) {
      $elements[] = [
        'key' => 'itunes:image',
        'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
        'attributes' => [
          'href' => $this->absoluteUrlGenerator->generateAbsoluteUrl($image_value),
        ],
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->getField(0, $this->options['description_field']);
  }

  /**
   * Gets owner rss item array.
   *
   * @return array
   *   The owner rss item array.
   */
  protected function getOwnerItem() {
    $nested_elements = [];
    $nested_elements[] = [
      '#theme' => 'podcast_publisher_nested_element',
      '#key' => 'itunes:name',
      '#value' => (string) $this->getField(0, $this->options['owner_field']),
    ];
    $nested_elements[] = [
      '#theme' => 'podcast_publisher_nested_element',
      '#key' => 'itunes:email',
      '#value' => (string) $this->getField(0, $this->options['email_field']),
    ];
    return [
      'key' => 'itunes:owner',
      'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
      'value' => $nested_elements,
    ];
  }

  /**
   * Gets itunes category rss item array.
   *
   * @param string $field_value
   *   The field value.
   *
   * @return array
   *   The itunes category rss item array.
   */
  protected function getItunesCategories($field_value) {
    $elements = [];
    $term_ids = explode(',', $field_value);
    /** @var \Drupal\taxonomy\TermInterface[] $terms */
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($term_ids);
    foreach ($terms as $term) {
      if ($parent = $term->get('parent')->entity) {
        $elements[] = [
          'key' => 'itunes:category',
          'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
          'attributes' => [
            'text' => $parent->label(),
          ],
          'value' => [
            '#theme' => 'podcast_publisher_nested_element',
            '#key' => 'itunes:category',
            '#attributes' => [
              'text' => $term->label(),
            ],
          ],
        ];
      }
      else {
        $elements[] = [
          'key' => 'itunes:category',
          'namespace' => ['xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd'],
          'attributes' => [
            'text' => $term->label(),
          ],
        ];
      }
    }
    return $elements;
  }

  /**
   * The podcast's title.
   *
   * @return string|null
   *   The title.
   */
  public function getTitle(): string {
    return (string) $this->getField(0, $this->options['title_field']);
  }

}
