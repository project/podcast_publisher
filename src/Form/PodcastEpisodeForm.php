<?php

namespace Drupal\podcast_publisher\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the podcast episode entity edit forms.
 */
class PodcastEpisodeForm extends ContentEntityForm {

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new self(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
    );
    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    if (!isset($form['advanced'])) {
      $form['advanced'] = [
        '#type' => 'vertical_tabs',
        '#weight' => 99,
      ];
    }

    // Podcast author information for administrators.
    $form['author_internal'] = [
      '#type' => 'details',
      '#title' => $this->t('Internal Authoring information'),
      '#group' => 'advanced',
      '#weight' => 90,
      '#optional' => TRUE,
    ];

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author_internal';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author_internal';
    }

    // Podcast author information for administrators.
    $form['author_feed'] = [
      '#type' => 'details',
      '#title' => $this->t('Authoring information for the feed'),
      '#group' => 'advanced',
      '#weight' => 0,
      '#optional' => TRUE,
    ];

    if (isset($form['authors'])) {
      $form['authors']['#group'] = 'author_feed';
    }

    if (isset($form['status'])) {
      $form['status']['#group'] = 'footer';
    }

    $form['duration']['#type'] = 'hidden';

    if ($form_state->getFormObject()->getFormId() == 'podcast_episode_add_form' && $podcast_id = (int) $this->getRequest()->query->get('podcast')) {
      $form['podcast']['widget']['#default_value'] = $podcast_id;
      $form['episode_number']['widget'][0]['value']['#default_value'] = $this->getIncrementedEpisodeNumber($podcast_id);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New podcast episode %label has been created.', $message_arguments));
        $this->logger('podcast_publisher')->notice('Created new podcast episode %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The podcast episode %label has been updated.', $message_arguments));
        $this->logger('podcast_publisher')->notice('Updated podcast episode %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.podcast_episode.canonical', ['podcast_episode' => $entity->id()]);

    return $result;
  }

  /**
   * Returns the last episode number plus one.
   *
   * @param int $podcast_id
   *   The podcast id.
   *
   * @return int|null
   *   The number of the next episode.
   */
  protected function getIncrementedEpisodeNumber(int $podcast_id): int {
    $episode_storage = $this->entityTypeManager->getStorage('podcast_episode');
    $last_episode_id = $episode_storage->getQuery()
      ->accessCheck(FALSE)
      ->sort('episode_number', 'DESC')
      ->range(0, 1)
      ->condition('podcast', $podcast_id)
      ->execute();

    if ($last_episode_id = reset($last_episode_id)) {
      $last_episode = $episode_storage->load($last_episode_id);
      return $last_episode->episode_number->value + 1;
    }

    return 1;
  }

}
