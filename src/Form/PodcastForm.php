<?php

namespace Drupal\podcast_publisher\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the podcast entity edit forms.
 */
class PodcastForm extends ContentEntityForm {

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new self(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
    );
    $instance->currentUser = $container->get('current_user');
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    if (!isset($form['advanced'])) {
      $form['advanced'] = [
        '#type' => 'vertical_tabs',
        '#weight' => 99,
      ];
    }

    // Podcast author information for administrators.
    $form['author_internal'] = [
      '#type' => 'details',
      '#title' => $this->t('Internal Authoring information'),
      '#group' => 'advanced',
      '#weight' => 90,
      '#optional' => TRUE,
    ];

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author_internal';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author_internal';
    }

    // Podcast author information for administrators.
    $form['author_feed'] = [
      '#type' => 'details',
      '#title' => $this->t('Authoring information for the feed'),
      '#group' => 'advanced',
      '#weight' => 80,
      '#optional' => TRUE,
    ];

    if (isset($form['authors'])) {
      $form['authors']['#group'] = 'author_feed';
    }

    if (isset($form['email'])) {
      $form['email']['#group'] = 'author_feed';
    }

    if (isset($form['status'])) {
      $form['status']['#group'] = 'footer';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New podcast %label has been created.', $message_arguments));
      $this->logger('podcast_publisher')->notice('Created new podcast %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The podcast %label has been updated.', $message_arguments));
      $this->logger('podcast_publisher')->notice('Updated new podcast %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.podcast.canonical', ['podcast' => $entity->id()]);
    return $result;
  }

}
